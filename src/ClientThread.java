
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.io.PrintWriter;
import java.net.Socket;
import java.io.IOException;
/**
 * Created by java on 30.01.2017.
 */
public class ClientThread extends Thread {
    private Socket socket;

    public ClientThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try (PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
                while (true){
                    String line = in.readLine();
                    for (Socket s : Server.clients) {
                        if (s != socket) {
                            PrintWriter o = new PrintWriter(s.getOutputStream(), true);
                            o.println(line);
                        }
                    }
                }


        }catch(Exception e) {
            Server.clients.remove(socket);
            System.out.println("disconnected client");


        }
    }
}
