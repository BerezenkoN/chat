

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by java on 30.01.2017.
 */
public class Client {
    static String hostName = "127.0.0.1";
static int portNumber = 5555;
public static void main(String[] args) throws IOException{
    try(Socket socket = new Socket(hostName, portNumber);
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in)))
    {
        String userInput;
        Thread reader = new Thread(()-> {
            try{
                while (true){
                    String msgFromServer = in.readLine();
                    System.out.println(msgFromServer);
                }
            }catch (IOException e){
                e.printStackTrace();
            }
            });
        reader.start();
        while (!"exit".equals(userInput = stdIn.readLine())){
            out.println(userInput);
        }
    }catch (UnknownHostException e){
        System.err.println("Unknown host" + hostName);
        System.exit(1);
    }catch (IOException e){
        System.err.println("Couldn't connection to" + hostName);
        System.exit(1);

    }


}
}






